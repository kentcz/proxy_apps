# proxy_apps workload suite #

This repo contains a suite of proxy app workloads.

The proxy apps:

* CoEVP
* ASPA
* RSBench
* XSBench
* SimpleMOC
* Nekbone
* Lassen
* UMT
* CoSP2
* VPFFT
* COHMM
* COMD
* LULESH
* MiniFE

Dependencies that are included:

* OpenBLAS
* PETSc
* Eigen
* FFTW



Checkout
-------
This Git repo contains submodules. Use "--recursive" clone the repo.
```
git clone --recursive git@bitbucket.org:kentcz/proxy_apps.git
```



Building
-------
Most of the setup is contained within the Makefile. The source code for the applications are either downloaded during the build or cloned as git submodules.

Use Make to build the applications.
```
make
```

Software dependencies:

* icc
* gcc/g++
* gfortran
* mpi

On Ubuntu, the dependencies can be installed with
```
sudo apt-get install gcc g++ gfortran mpi-default-dev mpi-default-bin
```

Running
-------
Most of the applications have a run target. For example, XSBench has a "run-XSBench" make target.
```
make run-XSBench
```