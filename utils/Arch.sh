#! /bin/bash

if [ `grep -c avx2 /proc/cpuinfo` -gt "0" ]; then
	echo HASWELL
elif [ `grep -c avx /proc/cpuinfo` -gt "0" ]; then
	echo SANDYBRIDGE
elif [ `grep -c sse4_2 /proc/cpuinfo` -gt "0" ]; then
	echo NEHALEM
elif [ `grep -c sse4_1 /proc/cpuinfo` -gt "0" ]; then
	echo PENRYN
else
	echo P2
fi
