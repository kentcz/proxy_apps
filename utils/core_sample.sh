#! /bin/bash

export OMP_NUM_THREADS=4

NUM_ARGS=$#
AUX_ARGS=""

for i in `seq 2 $NUM_ARGS`;
do
	AUX_ARGS="${AUX_ARGS} ${!i}"
done
echo $AUX_ARGS

sep -start -out sep_top.tb6 -ec \
	"INST_RETIRED.ANY", \
	"CPU_CLK_UNHALTED.THREAD", \
	"IDQ_UOPS_NOT_DELIVERED.CORE", \
	"UOPS_ISSUED.ANY", \
	"UOPS_RETIRED.RETIRE_SLOTS", \
	"INT_MISC.RECOVERY_CYCLES", \
	-ebc -em trigger="CPU_CLK_UNHALTED.REF_TSC" factor=1 \
	-app $1 \
	-args "${AUX_ARGS}"

sep -start -out sep_core.tb6 -ec \
	"CPU_CLK_UNHALTED.THREAD", \
	"CYCLE_ACTIVITY.CYCLES_NO_EXECUTE", \
	"UOPS_EXECUTED.CYCLES_GE_1_UOP_EXEC", \
	"UOPS_EXECUTED.CYCLES_GE_2_UOPS_EXEC", \
	"CYCLE_ACTIVITY.STALLS_LDM_PENDING", \
	"RESOURCE_STALLS.SB", \
	-ebc -em trigger="CPU_CLK_UNHALTED.REF_TSC" factor=1 \
	-app $1 \
	-args "${AUX_ARGS}"

#./mgm 1 12 5 0 1 100 y
