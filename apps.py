import os
import sys
import subprocess
import shlex

def experiment_name(app_name):
	global trial
	global arch
	global output_dir
	return output_dir + app_name + "_" + arch + "_" + str(trial) + ".log"


class App:
	app_name = ""
	bin_path = ""
	bin_name = ""
	bin_args = ""

	def __init__(self, app_name, bin_path, bin_args):
		self.app_name = app_name
		self.bin_path = bin_path
		self.bin_args = bin_args
	
	def run(self):
		global root_path
		wd = root_path +  self.bin_path
		args = shlex.split(self.bin_args)
		subprocess.call(args, cwd=wd)
		#
		logfile_name = experiment_name(self.app_name)
		logfile = open(logfile_name, 'w')
		proc = subprocess.Popen(args, cwd=wd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		for line in proc.stdout:
			sys.stdout.write(line)
			logfile.write(line)
		#proc.wait()
		#subprocess.call(['./miniFE.x', 'nx=100'], cwd=wd)


#
# Configuration
#
root_path = os.path.dirname(os.path.realpath(__file__)) + "/"
output_dir = root_path + "output/"
trial = 1
arch = "IVB"

# Set the root path
print root_path

# Add Apps
apps = {}
apps["LULESH"] = App("LULESH","LULESH/","./lulesh2.0")
apps["MiniFE"] = App("MiniFE","MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt/","./miniFE.x nx=100")


#apps["MiniFE"].run()
apps["LULESH"].run()
