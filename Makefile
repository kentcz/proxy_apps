# ISA level (sse, avx, or native)
ISA=native
MPICC = mpicc

FFTW_VERSION = fftw-3.3.4

# BLAS PATH
BLAS_PATH=${PWD}/libs/OpenBLAS/lib
BLASLIB=libs/OpenBLAS/lib/libopenblas.a
FFTW_PATH=${PWD}/libs/FFTW
PETSC_DIR=${PWD}/libs/PETSc
PETSC_ARCH=IVB


SAMPLE = ${PWD}/utils/core_sample.sh
SAMPLE_SINGLE = ${PWD}/utils/core_sample_single.sh

# Lib files
lib_files = ${BLASLIB} 
lib_files += libs/Eigen 
lib_files += libs/FFTW 

# Build files
build_files = ${lib_files}
build_files += CoMD/bin/CoMD-openmp-mpi
build_files += CoHMM/cohmm
build_files += CoEVP/LULESH/lulesh 
build_files += CoSP2/bin/CoSP2Parallel
build_files += Lassen/lassen_mpi
build_files += RSBench/src/rsbench 
build_files += XSBench/src/XSBench
build_files += Nekbone/test/example1/nekbone
build_files += Nekbone/test/example2/nekbone
build_files += Nekbone/test/example3/nekbone
build_files += SimpleMOC/src/SimpleMOC 
build_files += LULESH/lulesh2.0
build_files += MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt/miniFE.x
build_files += UMT2013/Teton/SuOlsonTest 
#build_files += ASPA/exec/aspa

all : ${build_files}

####################################################
# CoEVP 
####################################################
run-CoEVP : CoEVP/LULESH/lulesh 
	cd CoEVP/LULESH ; \
	./lulesh 
	# Serial ~30 sec

sep-CoEVP : CoEVP/LULESH/lulesh 
	cd CoEVP/LULESH ; \
	${SAMPLE_SINGLE} ./lulesh 

CoEVP/LULESH/lulesh : CoEVP/CM/lib/libcm.a ${BLASLIB}
	-patch -N CoEVP/LULESH/Makefile < patches/CoEVP/Makefile_lulesh.${ISA}.patch
	$(MAKE) -C CoEVP/LULESH/

CoEVP/CM/lib/libcm.a : ${BLASLIB}
	-patch -N CoEVP/CM/exec/Makefile < patches/CoEVP/Makefile.${ISA}.patch
	$(MAKE) -C CoEVP/CM/exec/

####################################################
# ASPA 
####################################################
run-ASPA : ASPA/exec/aspa
	cd ASPA/exec ; \
	OMP_NUM_THREADS=4 ./aspa point_data.txt value_data.txt

sep-ASPA : ASPA/exec/aspa
	cd ASPA/exec ; \
	${SAMPLE} ./aspa point_data.txt value_data.txt

ASPA/exec/aspa : ${BLASLIB} 
	-patch -N ASPA/exec/Makefile < patches/ASPA/Makefile.patch
	$(MAKE) -C ASPA/exec/

####################################################
# RSBench 
####################################################
run-RSBench : RSBench/src/rsbench
	cd RSBench/src/ ; \
	./rsbench -s small -t 4
	# -t <threads> -s <size: small, large>

sep-RSBench : RSBench/src/rsbench
	cd RSBench/src/ ; \
	${SAMPLE} ./rsbench -s small -t 4 

RSBench/src/rsbench : 
	-patch -N RSBench/src/makefile < patches/RSBench/makefile.${ISA}.patch
	$(MAKE) -C RSBench/src

####################################################
# XSBench 
####################################################
run-XSBench : XSBench/src/XSBench
	cd XSBench/src/ ; \
	./XSBench -t 4
	# -t <threads> -s <size: small, large>

sep-XSBench : XSBench/src/XSBench
	cd XSBench/src/ ; \
	${SAMPLE} ./XSBench -s small -t 4 

XSBench/src/XSBench : 
	-patch -N XSBench/src/Makefile < patches/XSBench/Makefile.${ISA}.patch
	$(MAKE) -C XSBench/src

####################################################
# SimpleMOC 
####################################################
run-SimpleMOC: SimpleMOC/src/SimpleMOC 
	cd SimpleMOC/src/ ; \
	./SimpleMOC -t 4
	# -t <threads> 
	#  mpirun -np 1 doesn't work
	#  Take 20 minutes ; Uses over 10 GB of RAM

SimpleMOC/src/SimpleMOC : 
	-patch -N SimpleMOC/src/Makefile < patches/SimpleMOC/Makefile.${ISA}.patch
	$(MAKE) -C SimpleMOC/src

####################################################
# Nekbone 
####################################################
run-Nekbone: Nekbone/test/example1/nekbone
	cd Nekbone/test/example1 ; \
	./nekbone ex1 
	# Serial
	# ./nekpmpi ex1 4

sep-Nekbone: Nekbone/test/example1/nekbone
	cd Nekbone/test/example1 ; \
	${SAMPLE} ./nekbone ex1 

Nekbone/test/example1/nekbone : 
	-patch -N Nekbone/test/example1/makenek < patches/Nekbone/example1/makenek.patch 
	cd Nekbone/test/example1 ; \
	./makenek ex1

Nekbone/test/example2/nekbone : 
	-patch -N Nekbone/test/example2/makenek < patches/Nekbone/example2/makenek.patch 
	cd Nekbone/test/example2 ; \
	./makenek ex2

Nekbone/test/example3/nekbone : 
	-patch -N Nekbone/test/example3/makenek < patches/Nekbone/example3/makenek.patch 
	cd Nekbone/test/example3 ; \
	./makenek ex3

####################################################
# Lassen 
####################################################
run-Lassen : Lassen/lassen_mpi 
	cd Lassen ; \
	mpirun -n 4 ./lassen_mpi default 2 2 2000 2000	

run-Lassen-Serial : Lassen/lassen_mpi 
	cd Lassen ; \
	./lassen_serial default 100 100 100	

sep-Lassen : Lassen/lassen_mpi 
	cd Lassen ; \
	${SAMPLE} mpirun -n 4 ./lassen_mpi default 2 2 2000 2000	
	#${SAMPLE} ./lassen_serial default 100 100 100	

Lassen :
	wget https://codesign.llnl.gov/downloads/lassen.v1.0.tar.gz 
	tar xzvf lassen.v1.0.tar.gz
	rm lassen.v1.0.tar.gz
	mv lassen Lassen

Lassen/lassen_mpi : Lassen
	-patch -N Lassen/Makefile < patches/Lassen/Makefile.${ISA}.patch
	-patch -N Lassen/Output.cxx < patches/Lassen/Output.cxx.patch
	$(MAKE) -C Lassen	

####################################################
# UMT 
####################################################

run-UMT2013 : UMT2013/Teton/SuOlsonTest 
	cd UMT2013/Teton ; \
	./SuOlsonTest problem1.cmg 16 2 16 8 4

sep-UMT2013 : UMT2013/Teton/SuOlsonTest 
	cd UMT2013/Teton ; \
	${SAMPLE} ./SuOlsonTest problem1.cmg 16 2 16 8 4


UMT2013 :
	wget https://asc.llnl.gov/CORAL-benchmarks/Throughput/UMT2013-20140204.tar.gz 
	tar xzvf UMT2013-20140204.tar.gz
	rm UMT2013-20140204.tar.gz

UMT2013/Teton/SuOlsonTest : UMT2013 
	-patch -N UMT2013/Teton/Makefile < patches/UMT2013/Makefile.patch
	-patch -N UMT2013/make.defs < patches/UMT2013/make.defs.${ISA}.patch
	$(MAKE) -C UMT2013
	$(MAKE) -C UMT2013/Teton SuOlsonTest

####################################################
# CoSP2 
####################################################
run-CoSP2Parallel : CoSP2/bin/CoSP2Parallel 
	cd CoSP2/examples ; \
	OMP_NUM_THREADS=4 ../bin/CoSP2Parallel --hmatName ${PWD}/CoSP2/data/hmatrix.1024.mtx -N 12288 -M 256

run-CoSP2 : CoSP2/bin/CoSP2
	cd CoSP2/examples ; \
	OMP_NUM_THREADS=4 ../bin/CoSP2 --hmatName ${PWD}/CoSP2/data/hmatrix.1024.mtx -N 12288 -M 256

sep-CoSP2Parallel : CoSP2/bin/CoSP2Parallel
	cd CoSP2/examples ; \
	${SAMPLE} ../bin/CoSP2Parallel --hmatName ${PWD}/CoSP2/data/hmatrix.1024.mtx -N 12288 -M 256

sep-CoSP2 : CoSP2/bin/CoSP2
	cd CoSP2/examples ; \
	${SAMPLE} ../bin/CoSP2 --hmatName ${PWD}/CoSP2/data/hmatrix.1024.mtx -N 12288 -M 256

CoSP2/bin/CoSP2Parallel :
	cp CoSP2/src-mpi/Makefile.vanilla CoSP2/src-mpi/Makefile
	-patch -N CoSP2/src-mpi/Makefile < patches/CoSP2/Makefile.${ISA}.patch
	-patch -N CoSP2/src-mpi/mycommand.c < patches/CoSP2/mycommand.c.patch
	$(MAKE) -C CoSP2/src-mpi clean
	$(MAKE) -C CoSP2/src-mpi

#CoSP2/bin/CoSP2 : 
#	cp CoSP2/src-mpi/Makefile.vanilla CoSP2/src-mpi/Makefile
#	#-patch -N CoSP2/src-mpi/Makefile < patches/CoSP2/Makefile.openmp.patch
#	-patch -N CoSP2/src-mpi/Makefile < patches/CoSP2/Makefile.patch
#	-patch -N CoSP2/src-mpi/mycommand.c < patches/CoSP2/mycommand.c.patch
#	$(MAKE) -C CoSP2/src-mpi clean
#	$(MAKE) -C CoSP2/src-mpi
#CoSP2/bin/CoSP2Parallel : CoSP2/bin/CoSP2
#	#cp CoSP2/src-mpi/Makefile.vanilla CoSP2/src-mpi/Makefile
#	#-patch -N CoSP2/src-mpi/mycommand.c < patches/CoSP2/mycommand.c.patch
#	$(MAKE) -C CoSP2/src-mpi clean
#	$(MAKE) -C CoSP2/src-mpi

####################################################
# VPFFT 
####################################################
run-VPFFT : VPFFT/VPFFT++
	cd VPFFT ; \
	OMP_NUM_THREADS=4 ./VPFFT++

VPFFT/VPFFT++ : libs/FFTW libs/Eigen
	-patch -N VPFFT/Makefile.make < patches/VPFFT/Makefile.make.patch
	cd VPFFT ; cp Makefile.make Makefile
	$(MAKE) -C VPFFT/


####################################################
#  CoHMM
####################################################
run-CoHMM : CoHMM/cohmm
	cd CoHMM ; \
	OMP_NUM_THREADS=4 ./cohmm 2

sep-CoHMM : CoHMM/cohmm
	cd CoHMM ; \
	${SAMPLE} ./cohmm 2

CoHMM/cohmm :
	-patch -N CoHMM/Makefile < patches/CoHMM/Makefile.${ISA}.patch
	mkdir -p CoHMM/output/simulation%i/adaptive/
	$(MAKE) -C CoHMM

####################################################
#  CoMD
####################################################
sep-CoMD : CoMD/bin/CoMD-openmp-mpi 
	cd CoMD ; \
	${SAMPLE} ./bin/CoMD-openmp-mpi -e -i 1 -j 1 -k 1 -x 40 -y 40 -z 40

run-CoMD : CoMD/bin/CoMD-openmp-mpi 
	cd CoMD ; \
	OMP_NUM_THREADS=4 ./bin/CoMD-openmp-mpi -e -i 1 -j 1 -k 1 -x 40 -y 40 -z 40

CoMD/Makefile : 
	cp CoMD/src-openmp/Makefile.vanilla CoMD/src-openmp/Makefile
	-patch -N CoMD/src-openmp/Makefile < patches/CoMD/Makefile.${ISA}.patch

CoMD/bin/CoMD-openmp-mpi : CoMD/Makefile
	$(MAKE) -C CoMD/src-openmp/

####################################################
# LULESH 
####################################################
run-LULESH : LULESH/lulesh2.0
	cd LULESH ; \
	./lulesh2.0

sep-LULESH : LULESH/lulesh2.0
	cd LULESH ; \
	${SAMPLE_SINGLE} ./lulesh2.0

LULESH/lulesh2.0 :
	mkdir -p LULESH
	wget -P LULESH/ https://codesign.llnl.gov/lulesh/lulesh2.0.3.tgz
	cd LULESH ; \
	tar xzvf lulesh2.0.3.tgz
	-patch -N LULESH/Makefile < patches/LULESH/Makefile.${ISA}.patch
	$(MAKE) -C LULESH

####################################################
# MiniFE 
####################################################
run-MiniFE : MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt/miniFE.x
	cd MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt/ ; \
	./miniFE.x nx=100

sep-MiniFE : MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt/miniFE.x
	cd MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt/ ; \
	${SAMPLE} ./miniFE.x nx=100

MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt/miniFE.x : 
	mkdir -p MiniFE
	wget -P MiniFE/ --no-check-certificate http://mantevo.org/downloads/releaseTarballs/miniapps/MiniFE/miniFE-2.0.1.tgz
	cd MiniFE ; \
	tar xzvf miniFE-2.0.1.tgz ; \
	tar xzvf miniFE-2.0.1_openmp_opt.tgz
	-patch -N MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt/Makefile.intel.openmp < patches/MiniFE/Makefile.intel.openmp.${ISA}.patch
	cd MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt ; \
	make -f Makefile.intel.openmp

####################################################
# OpenBLAS 
####################################################
libs/OpenBLAS/lib/libopenblas.a :
	mkdir -p libs/OpenBLAS
	${MAKE} -C libs/src/OpenBLAS/ TARGET=$(shell utils/Arch.sh)
	${MAKE} -C libs/src/OpenBLAS/ install PREFIX=../../OpenBLAS/

####################################################
# PETSc 
####################################################
libs/PETSc :
	rm -Rf libs/PETSc/
	wget -P libs/ https://bitbucket.org/petsc/petsc/get/v3.5.2.tar.gz
	cd libs/ ; \
	tar xzvf v3.5.2.tar.gz ; \
	rm v3.5.2.tar.gz ; \
	mv petsc-petsc-95571756d2fe PETSc
	cd libs/PETSc ; \
	PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${PETSC_ARCH} ./configure --with-debugging=1 --with-blas-lapack-dir=${BLAS_PATH} && \
	make PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${PETSC_ARCH} all && \
	make PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${PETSC_ARCH} test 

####################################################
# Eigen 
####################################################
libs/Eigen :
	wget -P libs/ http://bitbucket.org/eigen/eigen/get/3.2.2.tar.gz
	cd libs ; \
	tar xzvf 3.2.2.tar.gz ; \
	mv eigen-eigen-1306d75b4a21 Eigen ; \
	rm 3.2.2.tar.gz

####################################################
# FFTW 
####################################################
libs/FFTW :
	wget -P libs/src/ http://fftw.org/${FFTW_VERSION}.tar.gz 
	cd libs/src/ ; \
	tar xzvf ${FFTW_VERSION}.tar.gz ; \
	rm ${FFTW_VERSION}.tar.gz
	cd libs/src/${FFTW_VERSION} ; \
	./configure CC=gcc MPICC=${MPICC} --enable-$(shell utils/FFTW_ISA.sh) --enable-threads --enable-openmp --enable-mpi --prefix=${PWD}/libs/FFTW
	$(MAKE) -C libs/src/${FFTW_VERSION}
	$(MAKE) -C libs/src/${FFTW_VERSION} install

app_clean :
	git submodule foreach --recursive git clean -f -f
	# submodules
	rm -Rf ASPA/*
	rm -Rf CoEVP/*
	rm -Rf CoHMM/*
	rm -Rf CoMD/*
	rm -Rf CoSP2/*
	rm -Rf Nekbone/*
	rm -Rf RSBench/*
	rm -Rf XSBench/*
	rm -Rf SimpleMOC/*
	rm -Rf VPFFT/*
	rm -Rf libs/src/OpenBLAS/*
	git submodule foreach --recursive git reset --hard 
	# non-submodules
	rm -Rf LULESH/
	rm -Rf MiniFE/
	rm -Rf Lassen/
	rm -Rf UMT2013/
	
clean :
	git submodule foreach --recursive git clean -f -f
	# submodules
	rm -Rf ASPA/*
	rm -Rf CoEVP/*
	rm -Rf CoHMM/*
	rm -Rf CoMD/*
	rm -Rf CoSP2/*
	rm -Rf Nekbone/*
	rm -Rf RSBench/*
	rm -Rf XSBench/*
	rm -Rf SimpleMOC/*
	rm -Rf VPFFT/*
	rm -Rf libs/src/OpenBLAS/*
	git submodule foreach --recursive git reset --hard 
	# non-submodules
	rm -Rf LULESH/
	rm -Rf MiniFE/
	rm -Rf Lassen/
	rm -Rf UMT2013/
	rm -Rf libs/OpenBLAS/	
	rm -Rf libs/Eigen/
	rm -Rf libs/FFTW/
	rm -Rf libs/src/${FFTW_VERSION}
	rm -f libs/src/${FFTW_VERSION}.tar.gz
	rm -Rf libs/eigen-eigen-1306d75b4a21
	rm -f libs/3.2.2.tar.gz
