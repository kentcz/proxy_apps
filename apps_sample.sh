root_path=$PWD

# LULESH
app_name[0]="LULESH"
app_path[0]="LULESH/"
app_cmd[0]="./lulesh2.0 -q"
app_timer[0]=""

# MiniFE
app_name[1]="MiniFE"
app_path[1]="MiniFE/miniFE-2.0.1_openmp_opt/miniFE_openmp_opt/"
app_cmd[1]="./miniFE.x nx=200"
app_timer[1]="time"

# CoSP2
app_name[2]="CoSP2"
app_path[2]="CoSP2/examples/"
app_cmd[2]=" ../bin/CoSP2Parallel --hmatName ${PWD}/CoSP2/data/hmatrix.1024.mtx -N 12288 -M 256"
app_timer[2]=""

# CoHMM
app_name[3]="CoHMM"
app_path[3]="CoHMM/"
app_cmd[3]="./cohmm 2"
app_timer[3]=""

# CoMD
app_name[4]="CoMD"
app_path[4]="CoMD/"
app_cmd[4]="./bin/CoMD-openmp-mpi -e -i 1 -j 1 -k 1 -x 40 -y 40 -z 40"
app_timer[4]=""

# CoEVP
app_name[5]="CoEVP"
app_path[5]="CoEVP/LULESH/"
app_cmd[5]="./lulesh -t 4"
app_timer[5]=""

# XSBench
app_name[6]="XSBench"
app_path[6]="XSBench/src/"
app_cmd[6]="./XSBench -t 4"
app_timer[6]=""

# RSBench
app_name[7]="RSBench"
app_path[7]="RSBench/src/"
app_cmd[7]="./rsbench -s small -t 4"
app_timer[7]=""

# SimpleMOC
app_name[8]="SimpleMOC"
app_path[8]="SimpleMOC/src/"
app_cmd[8]="mpirun -np 4 ./SimpleMOC -t 4"
app_timer[8]=""

# Nekbone
app_name[9]="Nekbone"
app_path[9]="Nekbone/test/example1/"
app_cmd[9]="./nekpmpi ex1 4"
app_timer[9]=""

# Lassen
app_name[10]="Lassen"
app_path[10]="Lassen/"
app_cmd[10]="mpirun -n 4 ./lassen_mpi default 2 2 2000 2000"
app_timer[10]=""

# UMT
app_name[11]="UMT"
app_path[11]="UMT2013/Teton/"
app_cmd[11]="./SuOlsonTest problem1.cmg 16 2 16 8 4"
app_timer[11]=""

# VPFFT
app_name[12]="VPFFT"
app_path[12]="VPFFT/"
app_cmd[12]="./VPFFT++"
app_timer[12]=""

# ASPA
app_name[13]="ASPA"
app_path[13]="ASPA/exec/"
app_cmd[13]="./aspa point_data.txt value_data.txt"
app_timer[13]=""


exp=1

output_root=${root_path}/output
mkdir -p ${output_root}


for app in 0 1 2 3 4 5 6 7 9 10 11
do
	name=${app_name[$app]}
	path=${app_path[$app]}
	cmd=${app_cmd[$app]}
	timer=${app_timer[$app]}


	output_dir=${output_root}/${name}
	mkdir -p ${output_dir}

	cd ${root_path}/${path}
	echo name ${name}
	echo ${output_dir}/${name}.${exp}.log

	( time ${cmd} ) |& tee ${output_dir}/${name}.${exp}.log
	
	# Sample
	${root_path}/utils/core_sample.sh ${cmd}
	amplxe-cl -collect hotspots -loop-mode=loop-only -no-summary -r hotspots -- ${cmd}
	amplxe-cl -collect advanced-hotspots -loop-mode=loop-only -no-summary -r hotspots2 -- ${cmd}
	amplxe-cl -collect general-exploration -loop-mode=loop-only -no-summary -r general -- ${cmd}
	amplxe-cl -collect bandwidth -loop-mode=loop-only -no-summary -r bandwidth -- ${cmd}

	# Move data to output dir
	mv *.tb6 ${output_dir}/
	mv hotspots/ ${output_dir}/
	mv hotspots2/ ${output_dir}/
	mv general/ ${output_dir}/
	mv bandwidth/ ${output_dir}/

	cd ${output_dir}/
	amplxe-cl -report top-down -loop-mode=loop-only -r hotspots > hotspots_td.txt
	amplxe-cl -report hotspots -loop-mode=loop-only -r hotspots > hotspots_hs.txt
	amplxe-cl -report vecspots -loop-mode=loop-only -r hotspots > hotspots_vs.txt
	amplxe-cl -report callstacks -loop-mode=loop-only -r hotspots > hotspots_cs.txt
	amplxe-cl -report summary -r bandwidth > bandwidth_summary.txt
	amplxe-cl -report summary -r general > general_summary.txt
	amplxe-cl -report summary -r hotspots > hotspots_summary.txt
	amplxe-cl -report summary -r hotspots2 > hotspots2_summary.txt
	
	amplxe-cl -import sep_core.tb6 -r core
	amplxe-cl -import sep_top.tb6 -r top 
	amplxe-cl -report summary -r core > core_summary.txt
	amplxe-cl -report summary -r top > top_summary.txt
done
